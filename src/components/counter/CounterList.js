import Counter from "./Counter";

const CounterList = (props) => {

    const countersMap = props.counterIncrements.map(incrementValue => <Counter incrementStep={incrementValue} />);
    return (
        <>
            {countersMap}
        </>
    )
}

export default CounterList;
import { useState, useReducer } from "react";

export const INCREMENT_ONE = "increment_one";

const reducer = (state, action) => {
    switch (action.type) {
        case INCREMENT_ONE:
            return {
                ...state,
                counterValue: state.counterValue + 1,
                actionsCount: state.actionsCount + 1
            }
        case "DECREMENT_ONE":
            return {
                ...state,
                counterValue: state.counterValue - 1,
                actionsCount: state.actionsCount + 1
            }
        case "INCREMENT_N":
            return {
                ...state,
                actionsCount: state.actionsCount + 1,
                counterValue: state.counterValue + action.payload
            }
        case "DECREMENT_N":
            return {
                ...state,
                actionsCount: state.actionsCount + 1,
                counterValue: state.counterValue - action.payload
            }
        default:
            return state;
    }
}

const Counter = (props) => {
    const [state, dispatch] = useReducer(reducer, { counterValue: 0, actionsCount: 0 });

    // const [counterValue, setCounterValue] = useState(0);
    // const [actionsCount, setActionsCount] = useState(0);

    const incrementOneHandler = () => {
        dispatch({ type: INCREMENT_ONE });
        // setCounterValue(prevState => prevState + 1);
        // setActionsCount(prevState => ++prevState);
    }
    const incrementNHandler = () => {
        dispatch({ type: "INCREMENT_N", payload: props.incrementStep })
        // setCounterValue(prevState => prevState + props.incrementStep);
        // setActionsCount(prevState => ++prevState);
    }
    const decrementOneHandler = () => {
        dispatch({ type: "DECREMENT_ONE" });
        // setCounterValue(prevState => prevState - 1);
        // setActionsCount(prevState => ++prevState);
    }
    const decrementNHandler = () => {
        dispatch({ type: "DECREMENT_N", payload: props.incrementStep });
        // setCounterValue(prevState => prevState - props.incrementStep);
        // setActionsCount(prevState => ++prevState);
    }

    return (
        <div>
            <h2>Counter</h2>
            <div>{state.counterValue}</div>
            <div>{`Actions count: ${state.actionsCount}`}</div>
            <button onClick={incrementOneHandler}>Increment 1</button>
            <button onClick={incrementNHandler}>{`Increment ${props.incrementStep}`}</button>
            <button onClick={decrementOneHandler}>Decrement 1</button>
            <button onClick={decrementNHandler}>{`Decrement ${props.incrementStep}`}</button>
        </div>
    );
}

export default Counter;